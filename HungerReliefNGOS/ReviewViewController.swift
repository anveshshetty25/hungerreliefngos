//
//  ReviewViewController.swift
//  Hunger Relief NGO'S
//
//  Created by Valleshetti,Anwesh on 3/15/16.
//  Copyright © 2016 Valleshetti,Anwesh. All rights reserved.
//


import UIKit






class ReviewViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,Operation {
    
    var kinveyClientOperation:KinveyOperation!
    
    var array:[Item] = []
    var myitems:[Item] = []
    var userId:String!
    let defaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector:
            Selector("reloadData:"), name: "Data Delivered", object: nil)
        
        self.title = "Departments"
    
        self.navigationItem.title   = "Donate"
        
        let logout: UIBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("logout1:"))
       
        
        
        let cart: UIBarButtonItem = UIBarButtonItem(title: "cart", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("cart:"))
        
        self.navigationItem.setRightBarButtonItems([logout,cart], animated: true)
        
        
    
        //self.navigationItem.backBarButtonItem=nil
        kinveyClientOperation = KinveyOperation(operation:self)
         kinveyClientOperation.FetchItem(userId)
//        
//        var imageView:UIImageView
//        imageView = UIImageView(frame:CGRectMake(0, 0, 1000, 1000))
//        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "food.png")!)
//        self.view.addSubview(imageView)
        
        
        
    }
    func logout1(Any:AnyObject){
        
        if KCSUser.activeUser() != nil {
            
            KCSUser.activeUser().logout()
            
            //displayAlertControllerWithTitle("Success", message:"logged out!")
            
            let login =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("login") as! LoginViewController
            
            self.navigationController?.pushViewController(login, animated: true)
            
        }
    }
    func cart(Any:AnyObject){
        
     
        
        let map3 =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("cart") as! CartViewController
        
        self.navigationController?.pushViewController(map3, animated: true)
        
    }


    override func viewWillAppear(animated: Bool) {
        
        kinveyClientOperation.FetchItem(userId)
        self.tableView.reloadData()
    }
    //    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    //        if editingStyle == UITableViewCellEditingStyle.Delete {
    //            array.removeAtIndex(indexPath.row)
    //            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
    //        }
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return array.count
    }
    //    override func viewWillAppear(animated: Bool) {
    //        kinveyClientOperation.FetchItem()
    //    }
    
    func fetchitem(item: [Item]) {
        array =  item
        
        
        
        self.tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reviewcell", forIndexPath: indexPath)
        //kinveyClientOperation.FetchItem()
        
        
        
        
        let nameLbl : UILabel = cell.viewWithTag(11) as! UILabel
        
        let typeLbl : UILabel = cell.viewWithTag(12) as! UILabel // function viewWithTag(Int) to update the labels in the table.
        
        let quantityLbl:UILabel = cell.viewWithTag(78) as! UILabel
        
        nameLbl.text = array[indexPath.row].itemName
        
        typeLbl.text = array[indexPath.row].type
        
        quantityLbl.text = array[indexPath.row].quantity
        
        
        
        let stop : UIButton = UIButton(type: UIButtonType.RoundedRect) as UIButton
        
        stop.frame = CGRectMake(900, 0, 50, 50)
        
        //stop.center = CGPoint(x: view.bounds.width / 1.0, y: view.bounds.height / 1.0)
        
       
        
        let image = UIImage(named: "Shop") as UIImage?
        stop.setImage(image, forState:UIControlState.Normal)
        stop.tag = indexPath.row
        
        stop.addTarget(self, action: "Add:", forControlEvents:
            
            UIControlEvents.TouchUpInside)
        
        stop.setTitle("Add to Cart", forState: UIControlState.Normal)
        
        
        
        cell.addSubview(stop)
        
        return cell
    }
    
    func Add(sender:UIButton) {
        
        
        let indexPath  = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        
        myitems.append(array[indexPath.row])
        array.removeAtIndex(indexPath.row)
        tableView.reloadData()
        
        let uservalue = defaults.valueForKey(Constants.NGOS) as! String
        
        var cart = MyItems(itemName: array[indexPath.row].itemName, type: array[indexPath.row].type, quantity: array[indexPath.row].quantity, emailId: array[indexPath.row].emailId, userId: uservalue)
        kinveyClientOperation.SaveItem(cart)
}
}


