//
//  LoginViewController.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/5/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,Operation{
    
   
    
    @IBOutlet weak var EmailIdTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    var kinveyClientOperation:KinveyOperation!
    var  ngoslogin:NgosLogin!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyClientOperation = KinveyOperation(operation:self)
                 self.navigationItem.backBarButtonItem = nil
        self.navigationController?.navigationItem.title = "Login"
        self.navigationController?.navigationItem.backBarButtonItem = nil

        var imageView:UIImageView
        imageView = UIImageView(frame:CGRectMake(0, 0, 1000, 1000))
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "food.png")!)
        self.view.addSubview(imageView)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Login(sender: AnyObject) {
        
        if (EmailIdTF.text!.isEmpty || passwordTF.text!.isEmpty){
            self.displayAlertControllerWithTitle("Login Failed", message: "All fields are Required to fill")
        }
        else{
            ngoslogin = NgosLogin(emailId: EmailIdTF.text!, password: passwordTF.text!)
            kinveyClientOperation.login(ngoslogin)
           
        }
    }
    func loginFailed() {
        self.displayAlertControllerWithTitle("Login failed", message:"TryAgain")
    }
    func onSuccess(){
        self.displayAlertControllerWithSuccess("Login Successfull", message:"")
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func displayAlertControllerWithTitle(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
            message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    func displayAlertControllerWithSuccess(title:String, message:String) {
        print("login success")
        let uiAlertController:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{ action in self.performSegueWithIdentifier("loginSuccess", sender: self)}))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
        
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
