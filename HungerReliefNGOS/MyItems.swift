//
//  MyItem.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/15/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import Foundation



class MyItems:NSObject{
    var itemName:String
    var type:String
    var quantity:String
    var emailId:String
    var userId:String
    var entityId: String?
    
    
    override init(){
        self.itemName = ""
        self.type = ""
        self.quantity = ""
        self.emailId = ""
        self.userId = ""
        
    }
    
    init(itemName:String,type:String,quantity:String,emailId:String,userId:String){
        
        self.itemName = itemName
        self.type = type
        self.quantity = quantity
        self.emailId = emailId
        self.userId = userId
        
    }
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "itemName" : "itemName",
            "type" : "type",
            "quantity" : "quantity",
            "emailId" :"emailId",
            "userId":"userId"
            
        ]
    }
    
}
