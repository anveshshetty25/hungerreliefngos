//
//  NearestLocation.swift
//  HungerReliefNGOS
//
//  Created by Gaddam,Vishnu Tulasi on 4/13/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import Foundation

class NearestLocation:NSObject{
    var locations:[CLLocationCoordinate2D]!
    var entityId: String?
    
    init(locations:[CLLocationCoordinate2D]){
        self.locations = locations
    }
    override init(){
        self.locations = []
    }
   
    override func hostToKinveyPropertyMapping() -> [NSObject : AnyObject]! {
        return [
            "entityId" : KCSEntityKeyId, //the required _id field
            "locations":"locations"
            
        ]
    }

}