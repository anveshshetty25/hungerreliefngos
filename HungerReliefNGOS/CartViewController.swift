//
//  CartViewController.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/14/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import UIKit

class CartViewController: UIViewController ,Operation,UITableViewDataSource,UITableViewDelegate{
    var kinveyClientOperation:KinveyOperation!
    var arrayitems:[MyItems] = []
    let defaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyClientOperation = KinveyOperation(operation:self)
        let userValue = defaults.valueForKey(Constants.NGOS) as! String
        kinveyClientOperation.fetchMyItem(userValue)
        
        self.navigationController?.navigationItem.title = "MyCart"
        
        
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    func fetchmyitem(myitem: [MyItems]) {
        arrayitems = myitem
        
        
        self.tableView.reloadData()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return arrayitems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("table", forIndexPath: indexPath)
        
        let nameLbl : UILabel = cell.viewWithTag(3) as! UILabel
        
        let typeLbl : UILabel = cell.viewWithTag(2) as! UILabel // function viewWithTag(Int) to update the labels in the table.
        
        let quantityLbl:UILabel = cell.viewWithTag(8) as! UILabel
        
        nameLbl.text = arrayitems[indexPath.row].itemName
        
        typeLbl.text = arrayitems[indexPath.row].type
        
        quantityLbl.text = arrayitems[indexPath.row].quantity
        
        
        
        
        
        return cell
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */


}
