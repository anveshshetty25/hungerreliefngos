//
//  MyCartViewController.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/10/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import UIKit

class MyCartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,Operation {

    var kinveyClientOperation:KinveyOperation!

    var accessArray:[MyCart] = []
    
    @IBOutlet weak var tableView: UITableView!
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyClientOperation = KinveyOperation(operation:self)
        let userValue = defaults.valueForKey(Constants.NGOS) as! String
        kinveyClientOperation.fetchMyCart(userValue)
        
        
        
//        let c = self.tabBarController?.viewControllers![0] as! ItemsViewController
//        
//        displayArray = c.listarray
//        restuarant = c.restaurantName
//        
//      //  print(displayArray.name)
//        print(restuarant)
                
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func viewWillAppear(animated: Bool) {
        let userValue = defaults.valueForKey(Constants.NGOS) as! String
        kinveyClientOperation.fetchMyCart(userValue)
        self.tableView.reloadData()
    }
    func fetchCart(myCart: MyCart) {
        accessArray.append(myCart)
        print(accessArray)
        
        
        self.tableView.reloadData()
    }
    
    
   
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if accessArray.count == 0{
        return 1
        }
        else
        {
            return accessArray.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCellWithIdentifier("tablelist", forIndexPath: indexPath)
        let nameLbl : UILabel = cell.viewWithTag(100) as! UILabel
        let typeLbl:UILabel = cell.viewWithTag(102) as! UILabel
        let quantityLBl:UILabel = cell.viewWithTag(103) as! UILabel
        
       
        
        nameLbl.text = accessArray[indexPath.row].itemName
        
         typeLbl.text = accessArray[indexPath.row].type
        quantityLBl.text = accessArray[indexPath.row].quantity

        
       
        
      return cell
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
