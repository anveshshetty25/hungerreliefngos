//
//  ButtonCell.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/11/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import Foundation




class ButtonCell: UITableViewCell {
    
    var buttonDelegate: ButtonCellDelegate?
    
    @IBAction func buttonTap(sender: AnyObject) {
        if let delegate = buttonDelegate {
            delegate.cellTapped(self)
        }
    }
    
}