//
//  KinveyOperation.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/5/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import Foundation

@objc protocol Operation{
    
    optional func onSuccess()
    optional func onError(message:String)
    optional func noActiveUser()
    optional func loginFailed()
    optional func SignupSuccess()
    optional func Signupfailed()
    
    optional func fetch(restaurant:Restaurant)
    optional func fetchitem(item:[Item])
    optional func fetchmyitem(item:[MyItems])
    
}
class KinveyOperation{
    
        var ngos:Ngos!
    var ngosLogin:NgosLogin!
    let store:KCSAppdataStore!
    let store1:KCSAppdataStore!
    let store3:KCSAppdataStore!
  let store4:KCSAppdataStore!
    
    
    
    
    let operationDelegate:Operation!
    let defaults = NSUserDefaults.standardUserDefaults()
    
 
       init(operation:Operation){
        store = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "Ngos",
            KCSStoreKeyCollectionTemplateClass : Ngos.self
            ])
        store1 = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "Restaurant",
            KCSStoreKeyCollectionTemplateClass : Restaurant.self
            ])
        store3 = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "Item",
            KCSStoreKeyCollectionTemplateClass : Item.self
            ])
        store4 = KCSAppdataStore.storeWithOptions([
            KCSStoreKeyCollectionName : "MyItems",
            KCSStoreKeyCollectionTemplateClass : MyItems.self
            ])
      
        self.operationDelegate = operation
    }
    func saveData(){
        if let _ = KCSUser.activeUser(){
            
        }else{
            operationDelegate.noActiveUser!()
        }
    }
    
    
    func login(ngos:NgosLogin){
        KCSUser.loginWithUsername(
            ngos.emailId,
            password: ngos.password,
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil  {
                    
                    self.defaults.setValue(ngos.emailId, forKey: Constants.NGOS)
                    
                    self.defaults.synchronize()

                    self.operationDelegate.onSuccess!()
                } else {
                    
                    self.operationDelegate.loginFailed!()
                }
            }
        )
    }
    func Register(let n:Ngos){
        
        KCSUser.userWithUsername(
            n.emailId,
            password:n.password ,
            fieldsAndValues: [
                KCSUserAttributeGivenname :n.ngosName
            ],
            withCompletionBlock: { (user: KCSUser!, errorOrNil: NSError!, result: KCSUserActionResult) -> Void in
                if errorOrNil == nil {
                    self.operationDelegate.SignupSuccess!()
                    
                    
                    self.store.saveObject(
                        n,
                        withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                            if errorOrNil != nil {
                                //save failed
                                print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                            } else {
                                //save was successful
                                print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                            }
                        },
                        withProgressBlock: nil
                    )
                    
                    
                } else {
                    self.operationDelegate.Signupfailed!()
                    
                }
            }
        )
    }
    
       func FetchRestaurant() {
        //listArray = ""
       // print(KCSQuery())
        store1.queryWithQuery(
            KCSQuery(),
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
             // print(errorOrNil)
                if errorOrNil == nil {
                    if let objects = objectsOrNil as [AnyObject]!{
                        for object in objects{
                            let item = object as! Restaurant
                            self.operationDelegate.fetch!(item)
                            
                            
                            
                            //  print(item.itemName)
                            //
                            //
                            //                            self.listArray.append(item)
                            
                            //print(self.listArray.)
                        }
                    }
                    // print("****** \(self.listArray)")
                    
                } else {
                    
                    print("Error")
                    
                }
            },
            withProgressBlock: nil
        )
        
    }
    func FetchItem(emailId:String) {
       // let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "emailId", withExactMatchForValue: emailId)
               store3.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil == nil {
                    let objects = objectsOrNil as! [Item]
                    self.operationDelegate.fetchitem!(objects)
                    
                    
                
                } else {
                    
                    print("Error")
                    
                }
            },
            withProgressBlock: nil
        )
        
    }
        func fetchMyItem(emailId:String) {
        // let userValue = defaults.valueForKey(Constants.USERNAME) as! String
        let query = KCSQuery(onField: "userId", withExactMatchForValue: emailId)
        store4.queryWithQuery(
            query,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                
                if errorOrNil == nil {
                    let objects = objectsOrNil as! [MyItems]
                    self.operationDelegate.fetchmyitem!(objects)
                    
                    
                    
        
                    
                } else {
                    
                    print("Error")
                    
                }
            },
            withProgressBlock: nil
        )
        
    }
    func SaveItem(let myitem:MyItems){
        
        self.store4.saveObject(
            myitem,
            withCompletionBlock: { (objectsOrNil: [AnyObject]!, errorOrNil: NSError!) -> Void in
                if errorOrNil != nil {
                    //save failed
                    print("Save failed, with error: %@", errorOrNil.localizedFailureReason)
                } else {
                    //save was successful
                    print("Successfully saved event (id='%@').", (objectsOrNil[0] as! NSObject).kinveyObjectId())
                }
            },
            withProgressBlock: nil
        )
        
    }
    
   
    }



    

    

