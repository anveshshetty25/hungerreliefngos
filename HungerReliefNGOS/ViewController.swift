//
//  ViewController.swift
//  HungerReliefNGOS
//
//  Created by pratap on 3/18/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func displayAlertControllerWithTitle(title:String, message:String)
    {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
            message: message, preferredStyle: UIAlertControllerStyle.Alert); uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
                handler:{(action:UIAlertAction)->Void in  }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    
    @IBAction func signInBTN(sender: AnyObject) {
        displayAlertControllerWithTitle("Welcome", message: "You are successfully Signed In")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

