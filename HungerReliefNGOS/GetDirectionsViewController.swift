//
//  GetDirectionsViewController.swift
//  HungerReliefNGOS
//
//  Created by Priyanka on 4/16/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import UIKit
import MapKit

class GetDirectionsViewController: UIViewController {

    
    var restaurantAdd:String!
    var restaurantCity:String!
    var restaurantState:String!
    var restaurantZipCode:String!
    var restaurantName:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        let geoCoder = CLGeocoder()
        let addressString = "\(restaurantAdd) \(restaurantCity) \(restaurantState) \(restaurantZipCode)"
        
        geoCoder.geocodeAddressString(addressString, completionHandler:
            {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
                
                if error != nil {
                    print("Geocode failed with error: \(error!.localizedDescription)")
                } else if placemarks!.count > 0 {
                    print("hey")
                    
                    let placemark = placemarks![0] as! CLPlacemark
                    let location = placemark.location
                    let coords = location!.coordinate
                    let lat = (location?.coordinate.latitude)!
                    let long = (location?.coordinate.longitude)!
                    print(lat)
                    print(long)
                    print("hiiii")
                    let poin = MKPointAnnotation()
                    poin.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    self.mapView.addAnnotation(poin)
                    self.mapView.setCenterCoordinate(poin.coordinate, animated: true)
                    let span = MKCoordinateSpanMake(0.005, 0.005)
                    let region = MKCoordinateRegion(center: poin.coordinate, span: span)
                    self.mapView.setRegion(region, animated: true)
                    poin.title = self.restaurantName

                    
                    
                }
        })

    }

    @IBOutlet weak var mapView: MKMapView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
