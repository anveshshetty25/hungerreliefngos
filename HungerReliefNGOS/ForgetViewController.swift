//
//  ForgetViewController.swift
//  HungerReliefNGOS
//
//  Created by Kommula,Priyanka on 4/14/16.
//  Copyright © 2016 Valleshetti. All rights reserved.
//

import UIKit

class ForgetViewController: UIViewController,Operation {

    var kinveyOperations:KinveyOperation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        var imageView:UIImageView
//        imageView = UIImageView(frame:CGRectMake(0, 0, 1000, 1000))
//        imageView.image = UIImage(named:"backgroundImage.jpeg")
//        self.view.addSubview(imageView)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.title = "Forgot Password"
    }
    
    
    @IBOutlet weak var emailIDTF: UITextField!
    
    //@IBOutlet weak var lastNameTF: UITextField!
    
    @IBAction func submitButtonTapped(sender: AnyObject) {
        let emailID = emailIDTF.text!
        var message:String
        message = "Reset email sent to your inbox : \n \(emailID)"
        
        let title:String = "Forgot Password"
        print(emailID)
        
        KCSUser.sendPasswordResetForUser(emailID, withCompletionBlock: { (succeeded: Bool, error: NSError?) -> Void in
            if error == nil {
                if succeeded { // SUCCESSFULLY SENT TO EMAIL
                    print("Reset email sent to your inbox")
                    self.displayAlertControllerWithFailure(title, message: message)
                }
                else { // SOME PROBLEM OCCURED
                }
            }
            else { //ERROR OCCURED, DISPLAY ERROR MESSAGE
                print(error!.description)
            }
            
        })
        
        //        kinveyOperations.passwordReset(emailID)
    }
    
    
    func displayAlertControllerWithFailure(title:String, message:String) {
        let uiAlertController:UIAlertController = UIAlertController(title: title,
            message: message, preferredStyle: UIAlertControllerStyle.Alert)
        uiAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,
            handler:{(action:UIAlertAction)->Void in }))
        self.presentViewController(uiAlertController, animated: true, completion: nil)
    }
    


}
