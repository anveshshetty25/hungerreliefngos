//
//  ItemsViewController.swift
//  Hunger Relief NGO'S
//
//  Created by Valleshetti,Anwesh on 3/15/16.
//  Copyright © 2016 Valleshetti,Anwesh. All rights reserved.
//


import UIKit
import CoreLocation
import MapKit
class ItemsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource ,Operation{
    
    var kinveyClientOperation:KinveyOperation!
    var restaurant:Restaurant!
    var listarray:[Item] = []
    var restaurantName:String!
    
    @IBOutlet weak var tableView: UITableView!
    var restaurantArray:[Restaurant] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        kinveyClientOperation = KinveyOperation(operation:self)
        kinveyClientOperation.FetchRestaurant()
        
        self.navigationController?.navigationItem.title = "Restaurants"
        self.navigationController?.navigationItem.backBarButtonItem?.title = "Restaurants"
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func fetch(restaurant:Restaurant) {
        restaurantArray.append(restaurant)
        
        
        self.tableView.reloadData()
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return restaurantArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tablelist", forIndexPath: indexPath)
        //kinveyClientOperation.FetchItem()
        
        
        
        
        let nameLbl : UILabel = cell.viewWithTag(11) as! UILabel
        let addressLBL : UILabel = cell.viewWithTag(12) as! UILabel
        
        let stop : UIButton = UIButton(type: UIButtonType.RoundedRect) as UIButton
        
        stop.frame = CGRectMake(500, 0, 50, 50)
        
        //stop.center = CGPoint(x: view.bounds.width / 1.0, y: view.bounds.height / 1.0)
        
        
        
        let image = UIImage(named: "Shop") as UIImage?
        stop.setImage(image, forState:UIControlState.Normal)
        stop.tag = indexPath.row
        
        stop.addTarget(self, action: "Add:", forControlEvents:
            
            UIControlEvents.TouchUpInside)
        
        stop.setTitle("Add to Cart", forState: UIControlState.Normal)
        
        
        
        cell.addSubview(stop)

        
        nameLbl.text = restaurantArray[indexPath.row].restaurantName
        addressLBL.text = "\(restaurantArray[indexPath.row].address),\(restaurantArray[indexPath.row].city),\(restaurantArray[indexPath.row].state),\(restaurantArray[indexPath.row].country),\(restaurantArray[indexPath.row].zipCode) "
        
        return cell
    }
    func Add(sender:UIButton) {
        
        
        let indexPath  = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        
        let map =  self.navigationController?.storyboard?.instantiateViewControllerWithIdentifier("map") as! GetDirectionsViewController
        map.restaurantAdd = restaurantArray[indexPath.row].address
        map.restaurantCity  = restaurantArray[indexPath.row].city
          map.restaurantState  = restaurantArray[indexPath.row].state
           map.restaurantZipCode = restaurantArray[indexPath.row].zipCode
           map.restaurantName = restaurantArray[indexPath.row].restaurantName
        
        self.navigationController?.pushViewController(map, animated: true)

        
    }
  
      override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "review"
        {
            if let destinationVC = segue.destinationViewController as? ReviewViewController{
                if let displayIndex = tableView.indexPathForSelectedRow?.row {
                   
                    destinationVC.userId = restaurantArray[displayIndex].emailId
                    
                    restaurantName = restaurantArray[displayIndex].restaurantName
                    
                    
                }
               
            }
        }
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
